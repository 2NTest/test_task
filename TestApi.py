import requests
import unittest
import json
url = 'http://qainterview.cogniance.com/candidates'
data = {'name':'Karl','position':'June'}
headers = {'content-type':'application/json'}

class TestApi(unittest.TestCase):
	def test_add_cand(self):
		resp = requests.post(url, json = data, headers = headers)
		value = resp.json()
		Nid = value[u'candidate'][u'id'] 
		requests.delete("{}/{}".format(url, Nid))
		self.assertEqual(resp.status_code, 201)
	def test_added(self):
		oldCands = requests.get(url).text.count('id')
		addCand = requests.post(url, json = data, headers = headers)
		value = addCand.json()
		newCands = requests.get(url).text.count('id')
		Nid = value[u'candidate'][u'id'] 
		requests.delete("{}/{}".format(url, Nid))
		self.assertNotEqual(oldCands, newCands)
	def test_add_cand_noHead (self):
		resp = requests.post(url,json = data)
		value = resp.json()
		Nid = value[u'candidate'][u'id'] 
		requests.delete("{}/{}".format(url, Nid))
		self.assertEqual(resp.status_code, 400)
	def test_add_cand_noName(self):
		data = {'position':'JuniorQA'}
		resp = requests.post(url, json = data, headers = headers)
		self.assertEqual(resp.status_code, 400)
	def test_get_all(self):
		resp = requests.get(url)
		self.assertEqual(resp.status_code, 200)
	def test_get_cand_byid(self):
		NewAdd = requests.post(url, json = data, headers = headers)
		newCand = NewAdd.json()
		Nid = newCand[u'candidate'][u'id']
		resp = requests.get("{}/{}".format(url, Nid))
		gotCand = resp.json()
		requests.delete("{}/{}".format(url, Nid))
		self.assertEqual(resp.status_code, 200)
		self.assertEqual(newCand, gotCand)
	def test_dell_cand(self):
		Nadd = requests.post(url, json = data, headers = headers)
		value = Nadd.json()
		Nid = value[u'candidate'][u'id']
		resp = requests.delete("{}/{}".format(url, Nid))
		self.assertEqual(resp.status_code, 200)
if __name__ == '__main__':
    unittest.main()
    
    