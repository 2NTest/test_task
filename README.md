# Task for QA intern position#


## Project ##
My project for Cogniance consists of:

* TestApi.py - Tests that verifies that data is added to server and verifies correctness of returned response codes. Api documentation is given.

* Data.py - Reads existing access log file 'datamining.log' and creates a 'results.txt' file with the # of successful requests per hour.

## Api documentation ##

Server allowed methods:

GET, http://qainterview.cogniance.com/candidates, gives a list of all candidates. Returns 200.

GET, http://qainterview.cogniance.com/candidates/<cand_id>, shows a candidate with id=<cand_id>. Returns 200

POST, http://qainterview.cogniance.com/candidates, adds a new candidate. Returns 201. Case when header Content­Type: application/json or name is absent, 400 is returned.

DELETE, http://qainterview.cogniance.com/candidates/<cand_id>, deletes a candidate with id=<cand_id>. Returns 200


## Pre Conditions ##

* Python 2.7
* Modules for python installed: requests, unittest, json
* Path to your python folder is added to your system PATH variable (for Windows)

## Steps to Run ##

* Download all files from project into new directory
* Launch command line
* Go to the folder with the project
* Run: 
    python TestApi.py 
    python data.py 
* Check results of tests in command line
* Check results of datamining in 'results.txt' file in the folder with the project